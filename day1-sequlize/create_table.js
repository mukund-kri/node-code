const { Sequelize, Model, DataTypes } = require('sequelize');
const sequelize = new Sequelize('postgres://testuser:1234@localhost:5432/company_master')

const User = sequelize.define('user', {

  name: DataTypes.TEXT,
  favoriteColor: {
    type: DataTypes.TEXT,
    defaultValue: 'green'
  },
  age: DataTypes.INTEGER,
  cash: DataTypes.INTEGER
});


(async () => {
  await sequelize.sync({force: true});
  
})();
