const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('postgres://testuser:1234@localhost:5432/company_master')


async function checkConnection() {
  try {
    await sequelize.authenticate();
    console.log("success");
  } catch (error) {
    console.error('Unable to connect to db');
  }
}


checkConnection()
